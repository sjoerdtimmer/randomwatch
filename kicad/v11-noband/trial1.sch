EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date "27 apr 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ATTINY13-P IC1
U 1 1 5509AEFF
P 4900 3750
F 0 "IC1" H 4100 4150 40  0000 C CNN
F 1 "ATTINY13-P" H 5550 3350 40  0000 C CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_LongPads" H 5550 3750 35  0000 C CIN
F 3 "~" H 4100 4100 60  0000 C CNN
	1    4900 3750
	1    0    0    -1  
$EndComp
$Comp
L SW_PUSH SW1
U 1 1 5509C2E7
P 4200 4300
F 0 "SW1" H 4350 4410 50  0000 C CNN
F 1 "SW_PUSH" H 4200 4220 50  0000 C CNN
F 2 "Buttons_Switches_ThroughHole:SW_PUSH_SMALL" H 4200 4300 60  0000 C CNN
F 3 "~" H 4200 4300 60  0000 C CNN
	1    4200 4300
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR01
U 1 1 5517D2BA
P 6000 4150
F 0 "#PWR01" H 6000 4150 30  0001 C CNN
F 1 "GND" H 6000 4080 30  0001 C CNN
F 2 "" H 6000 4150 60  0000 C CNN
F 3 "" H 6000 4150 60  0000 C CNN
	1    6000 4150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 5517D2FB
P 4550 4350
F 0 "#PWR02" H 4550 4350 30  0001 C CNN
F 1 "GND" H 4550 4280 30  0001 C CNN
F 2 "" H 4550 4350 60  0000 C CNN
F 3 "" H 4550 4350 60  0000 C CNN
	1    4550 4350
	1    0    0    -1  
$EndComp
$Comp
L DS1302 U1
U 1 1 5517D412
P 4500 2000
F 0 "U1" H 4770 2550 60  0000 C CNN
F 1 "DS1302" H 4860 1450 60  0000 C CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_LongPads" H 4500 2000 60  0001 C CNN
F 3 "" H 4500 2000 60  0000 C CNN
	1    4500 2000
	1    0    0    -1  
$EndComp
$Comp
L CRYSTAL X1
U 1 1 5517D4BD
P 3050 2400
F 0 "X1" H 3050 2550 60  0000 C CNN
F 1 "CRYSTAL" H 3050 2250 60  0000 C CNN
F 2 "Crystals:Crystal_Round_Vertical_2mm" H 3050 2400 60  0000 C CNN
F 3 "~" H 3050 2400 60  0000 C CNN
	1    3050 2400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 5517D58F
P 4500 2750
F 0 "#PWR03" H 4500 2750 30  0001 C CNN
F 1 "GND" H 4500 2680 30  0001 C CNN
F 2 "" H 4500 2750 60  0000 C CNN
F 3 "" H 4500 2750 60  0000 C CNN
	1    4500 2750
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 5517D737
P 5350 2450
F 0 "#PWR04" H 5350 2450 30  0001 C CNN
F 1 "GND" H 5350 2380 30  0001 C CNN
F 2 "" H 5350 2450 60  0000 C CNN
F 3 "" H 5350 2450 60  0000 C CNN
	1    5350 2450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR05
U 1 1 5517D746
P 5350 2450
F 0 "#PWR05" H 5350 2450 30  0001 C CNN
F 1 "GND" H 5350 2380 30  0001 C CNN
F 2 "" H 5350 2450 60  0000 C CNN
F 3 "" H 5350 2450 60  0000 C CNN
	1    5350 2450
	1    0    0    -1  
$EndComp
$Comp
L BATTERY BT1
U 1 1 55182120
P 5600 950
F 0 "BT1" H 5600 1150 50  0000 C CNN
F 1 "BATTERY" H 5600 760 50  0000 C CNN
F 2 "Connect:CR2032H" H 5600 950 60  0000 C CNN
F 3 "~" H 5600 950 60  0000 C CNN
	1    5600 950 
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR06
U 1 1 551822BB
P 6050 1200
F 0 "#PWR06" H 6050 1200 30  0001 C CNN
F 1 "GND" H 6050 1130 30  0001 C CNN
F 2 "" H 6050 1200 60  0000 C CNN
F 3 "" H 6050 1200 60  0000 C CNN
	1    6050 1200
	1    0    0    -1  
$EndComp
Text Label 3450 2200 0    60   ~ 0
X1
Text Label 3450 2400 0    60   ~ 0
X2
$Comp
L LED H2
U 1 1 5538E036
P 8400 2450
F 0 "H2" H 8400 2550 50  0000 C CNN
F 1 "LED" H 8400 2350 50  0000 C CNN
F 2 "LEDs:LED-3MM" H 8400 2450 60  0000 C CNN
F 3 "~" H 8400 2450 60  0000 C CNN
	1    8400 2450
	0    1    1    0   
$EndComp
$Comp
L LED M8
U 1 1 5538E043
P 9400 2750
F 0 "M8" H 9400 2850 50  0000 C CNN
F 1 "LED" H 9400 2650 50  0000 C CNN
F 2 "LEDs:LED-3MM" H 9400 2750 60  0000 C CNN
F 3 "~" H 9400 2750 60  0000 C CNN
	1    9400 2750
	0    1    1    0   
$EndComp
$Comp
L LED H8
U 1 1 5538E049
P 9050 2750
F 0 "H8" H 9050 2850 50  0000 C CNN
F 1 "LED" H 9050 2650 50  0000 C CNN
F 2 "LEDs:LED-3MM" H 9050 2750 60  0000 C CNN
F 3 "~" H 9050 2750 60  0000 C CNN
	1    9050 2750
	0    -1   -1   0   
$EndComp
$Comp
L LED M2
U 1 1 5538E077
P 8750 2450
F 0 "M2" H 8750 2550 50  0000 C CNN
F 1 "LED" H 8750 2350 50  0000 C CNN
F 2 "LEDs:LED-3MM" H 8750 2450 60  0000 C CNN
F 3 "~" H 8750 2450 60  0000 C CNN
	1    8750 2450
	0    -1   -1   0   
$EndComp
$Comp
L LED H4
U 1 1 5538E0B4
P 8400 3000
F 0 "H4" H 8400 3100 50  0000 C CNN
F 1 "LED" H 8400 2900 50  0000 C CNN
F 2 "LEDs:LED-3MM" H 8400 3000 60  0000 C CNN
F 3 "~" H 8400 3000 60  0000 C CNN
	1    8400 3000
	0    1    1    0   
$EndComp
$Comp
L LED M4
U 1 1 5538E0BA
P 8750 3000
F 0 "M4" H 8750 3100 50  0000 C CNN
F 1 "LED" H 8750 2900 50  0000 C CNN
F 2 "LEDs:LED-3MM" H 8750 3000 60  0000 C CNN
F 3 "~" H 8750 3000 60  0000 C CNN
	1    8750 3000
	0    -1   -1   0   
$EndComp
$Comp
L R R0
U 1 1 5538E38F
P 7750 1700
F 0 "R0" V 7830 1700 40  0000 C CNN
F 1 "R" V 7757 1701 40  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 7680 1700 30  0000 C CNN
F 3 "~" H 7750 1700 30  0000 C CNN
	1    7750 1700
	0    -1   -1   0   
$EndComp
$Comp
L R R1
U 1 1 5538E3A6
P 7750 2200
F 0 "R1" V 7830 2200 40  0000 C CNN
F 1 "R" V 7757 2201 40  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 7680 2200 30  0000 C CNN
F 3 "~" H 7750 2200 30  0000 C CNN
	1    7750 2200
	0    -1   -1   0   
$EndComp
$Comp
L R R2
U 1 1 5538E3AC
P 7750 2750
F 0 "R2" V 7830 2750 40  0000 C CNN
F 1 "R" V 7757 2751 40  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 7680 2750 30  0000 C CNN
F 3 "~" H 7750 2750 30  0000 C CNN
	1    7750 2750
	0    -1   -1   0   
$EndComp
$Comp
L R R3
U 1 1 5538E3B2
P 7750 3250
F 0 "R3" V 7830 3250 40  0000 C CNN
F 1 "R" V 7757 3251 40  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 7680 3250 30  0000 C CNN
F 3 "~" H 7750 3250 30  0000 C CNN
	1    7750 3250
	0    -1   -1   0   
$EndComp
$Comp
L LED M1
U 1 1 5538E45D
P 8750 1950
F 0 "M1" H 8750 2050 50  0000 C CNN
F 1 "LED" H 8750 1850 50  0000 C CNN
F 2 "LEDs:LED-3MM" H 8750 1950 60  0000 C CNN
F 3 "~" H 8750 1950 60  0000 C CNN
	1    8750 1950
	0    -1   -1   0   
$EndComp
$Comp
L LED H1
U 1 1 5538E463
P 8400 1950
F 0 "H1" H 8400 2050 50  0000 C CNN
F 1 "LED" H 8400 1850 50  0000 C CNN
F 2 "LEDs:LED-3MM" H 8400 1950 60  0000 C CNN
F 3 "~" H 8400 1950 60  0000 C CNN
	1    8400 1950
	0    1    1    0   
$EndComp
$Comp
L LED M32
U 1 1 5538E53C
P 10050 2450
F 0 "M32" H 10050 2550 50  0000 C CNN
F 1 "LED" H 10050 2350 50  0000 C CNN
F 2 "LEDs:LED-3MM" H 10050 2450 60  0000 C CNN
F 3 "~" H 10050 2450 60  0000 C CNN
	1    10050 2450
	0    -1   -1   0   
$EndComp
$Comp
L LED M16
U 1 1 5538E542
P 9700 2450
F 0 "M16" H 9700 2550 50  0000 C CNN
F 1 "LED" H 9700 2350 50  0000 C CNN
F 2 "LEDs:LED-3MM" H 9700 2450 60  0000 C CNN
F 3 "~" H 9700 2450 60  0000 C CNN
	1    9700 2450
	0    1    1    0   
$EndComp
Text Notes 7330 7540 0    60   ~ 0
RandomWatch
Text Notes 10620 7660 0    60   ~ 0
8
Wire Wire Line
	5900 4000 6000 4000
Wire Wire Line
	6000 4000 6000 4150
Wire Wire Line
	3800 4300 3900 4300
Wire Wire Line
	4550 4350 4550 4300
Wire Wire Line
	4550 4300 4500 4300
Wire Wire Line
	3650 2200 2700 2200
Wire Wire Line
	4500 2750 4500 2600
Wire Wire Line
	5350 2300 5350 2450
Wire Wire Line
	2700 2200 2700 2400
Wire Wire Line
	2700 2400 2750 2400
Wire Wire Line
	3350 2400 3650 2400
Wire Wire Line
	1500 4000 3900 4000
Wire Wire Line
	6050 950  6050 1200
Wire Wire Line
	6050 950  5900 950 
Wire Wire Line
	8000 1700 10050 1700
Wire Wire Line
	10050 1700 10050 2250
Wire Wire Line
	10050 3250 10050 2650
Wire Wire Line
	8000 3250 10050 3250
Wire Wire Line
	9700 2250 9700 1700
Connection ~ 9700 1700
Wire Wire Line
	9700 2650 9700 3250
Connection ~ 9700 3250
Wire Wire Line
	9400 2200 9400 2550
Wire Wire Line
	8000 2200 9400 2200
Wire Wire Line
	9400 2950 9400 3250
Connection ~ 9400 3250
Wire Wire Line
	9050 2950 9050 3250
Connection ~ 9050 3250
Wire Wire Line
	9050 2550 9050 2200
Connection ~ 9050 2200
Wire Wire Line
	8750 1750 8750 1700
Connection ~ 8750 1700
Wire Wire Line
	8400 1750 8400 1700
Connection ~ 8400 1700
Wire Wire Line
	8750 2150 8750 2250
Connection ~ 8750 2200
Wire Wire Line
	8400 2150 8400 2250
Connection ~ 8400 2200
Wire Wire Line
	8750 2650 8750 2800
Wire Wire Line
	8000 2750 8750 2750
Wire Wire Line
	8400 2650 8400 2800
Connection ~ 8400 2750
Connection ~ 8750 2750
Wire Wire Line
	8750 3200 8750 3250
Connection ~ 8750 3250
Wire Wire Line
	8400 3200 8400 3250
Connection ~ 8400 3250
Wire Wire Line
	3800 4000 3800 4300
Text GLabel 7125 1700 0    60   Input ~ 0
L1
Wire Wire Line
	7125 1700 7500 1700
Text GLabel 7125 2200 0    60   Input ~ 0
L2
Text GLabel 7125 2750 0    60   Input ~ 0
L3
Text GLabel 7125 3250 0    60   Input ~ 0
L4
Wire Wire Line
	7125 2200 7500 2200
Wire Wire Line
	7125 2750 7500 2750
Wire Wire Line
	7125 3250 7500 3250
Text GLabel 3000 3500 0    60   Input ~ 0
L4
Wire Wire Line
	3900 3500 3000 3500
Text GLabel 3200 3600 0    60   Input ~ 0
L3
Wire Wire Line
	3900 3600 3200 3600
Text GLabel 3000 3700 0    60   Input ~ 0
L2
Wire Wire Line
	3000 3700 3900 3700
Text GLabel 3200 3800 0    60   Input ~ 0
L1
Wire Wire Line
	3200 3800 3900 3800
Text GLabel 3325 3325 1    60   Input ~ 0
DS1302_EN
Wire Wire Line
	3325 3325 3325 3900
Wire Wire Line
	3325 3900 3900 3900
Text GLabel 3675 3325 1    60   Input ~ 0
DS1302_SCK
Connection ~ 3675 3700
Text GLabel 3400 1700 0    60   Input ~ 0
DS1302_SCK
Text GLabel 3400 1900 0    60   Input ~ 0
DS1302_EN
Wire Wire Line
	3400 1700 3650 1700
Wire Wire Line
	3650 1900 3400 1900
Text GLabel 5725 1700 2    60   Input ~ 0
DS1302_DATA
Wire Wire Line
	5350 1700 5725 1700
Text GLabel 3500 3325 1    60   Input ~ 0
DS1302_DATA
Wire Wire Line
	3675 3325 3675 3700
Wire Wire Line
	3500 3325 3500 3800
Connection ~ 3500 3800
$Comp
L VCC #PWR07
U 1 1 56478B01
P 5050 650
F 0 "#PWR07" H 5050 750 30  0001 C CNN
F 1 "VCC" H 5050 750 30  0000 C CNN
F 2 "" H 5050 650 60  0000 C CNN
F 3 "" H 5050 650 60  0000 C CNN
	1    5050 650 
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 650  5050 950 
Wire Wire Line
	5050 950  5300 950 
$Comp
L VCC #PWR08
U 1 1 56478BA2
P 4500 1225
F 0 "#PWR08" H 4500 1325 30  0001 C CNN
F 1 "VCC" H 4500 1325 30  0000 C CNN
F 2 "" H 4500 1225 60  0000 C CNN
F 3 "" H 4500 1225 60  0000 C CNN
	1    4500 1225
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 1400 4500 1225
$Comp
L VCC #PWR09
U 1 1 56478CEA
P 6125 3250
F 0 "#PWR09" H 6125 3350 30  0001 C CNN
F 1 "VCC" H 6125 3350 30  0000 C CNN
F 2 "" H 6125 3250 60  0000 C CNN
F 3 "" H 6125 3250 60  0000 C CNN
	1    6125 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6125 3250 6125 3500
Wire Wire Line
	6125 3500 5900 3500
$Comp
L AVR-ISP-6 CON1
U 1 1 56479519
P 1775 3200
F 0 "CON1" H 1695 3440 50  0000 C CNN
F 1 "AVR-ISP-6" H 1535 2970 50  0000 L BNN
F 2 "Pin_Headers:Pin_Header_Straight_2x03" V 1255 3240 50  0001 C CNN
F 3 "" H 1775 3200 60  0000 C CNN
	1    1775 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1900 3200 3150 3200
Wire Wire Line
	3150 3200 3150 3500
Connection ~ 3150 3500
Wire Wire Line
	1650 3100 1300 3100
Wire Wire Line
	1300 3100 1300 3575
Wire Wire Line
	1300 3575 3275 3575
Wire Wire Line
	3275 3575 3275 3600
Connection ~ 3275 3600
Wire Wire Line
	1650 3200 1400 3200
Wire Wire Line
	1400 3200 1400 3800
Wire Wire Line
	1400 3800 3050 3800
Wire Wire Line
	3050 3800 3050 3700
Connection ~ 3050 3700
Wire Wire Line
	1650 3300 1500 3300
Wire Wire Line
	1500 3300 1500 4000
Connection ~ 3800 4000
$Comp
L GND #PWR010
U 1 1 56479753
P 2125 3425
F 0 "#PWR010" H 2125 3425 30  0001 C CNN
F 1 "GND" H 2125 3355 30  0001 C CNN
F 2 "" H 2125 3425 60  0000 C CNN
F 3 "" H 2125 3425 60  0000 C CNN
	1    2125 3425
	1    0    0    -1  
$EndComp
Wire Wire Line
	2125 3425 2125 3300
Wire Wire Line
	2125 3300 1900 3300
$Comp
L VCC #PWR011
U 1 1 56479806
P 2125 2925
F 0 "#PWR011" H 2125 3025 30  0001 C CNN
F 1 "VCC" H 2125 3025 30  0000 C CNN
F 2 "" H 2125 2925 60  0000 C CNN
F 3 "" H 2125 2925 60  0000 C CNN
	1    2125 2925
	1    0    0    -1  
$EndComp
Wire Wire Line
	2125 2925 2125 3100
Wire Wire Line
	2125 3100 1900 3100
$EndSCHEMATC
