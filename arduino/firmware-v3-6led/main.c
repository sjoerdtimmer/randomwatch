/**
 * This is not arduino code but "bare gcc". 
 * Easiest compilation is with the "bare gcc" option in Stino (sublime text plugin)
**/

#include "main.h"
#include <avr/sleep.h>
#include <avr/power.h>
// #include <EEPROM.h>  // depricated, I use eeprom on the ds1302 because eeprom.h is big and we already have the function for writing/reading to ds1302


#define DS1302_SCLK_PIN   PB2    // pin for the Serial Clock
#define DS1302_IO_PIN     PB3    // pin for the Data I/O
#define DS1302_CE_PIN     PB4    // pin for the Chip Enable


#define ITERATIONS 500
#define CHARLIEPLEXDELAY 1


// if defined, compiler time will be set on every boot, only usefull if you flash once with and once without
// #define SET_INITIAL_TIME

// if set, the time can be set with the reset button in the following manner:
// press the reset button while the leds are still on and the minutes will be bumped by 1
// press the reset button in the 1-second window after the leds turn off and the hours will be bumped by one
#define SELF_SETTING_OF_TIME_THROUGH_QUICK_RESETS
#define QUICK_RESET_MAGIC_ADDRESS 0xC0
/* This work as follows: on boot write a 3 to the magic eeprom address,
   after flashing the leds write a 2 to that address,
   after another second write a 0 to that address.
   On boot, read the magic address: depending on the value you'll know how long ago the previous boot was
*/

// if set, the magic address will be rotated to save eeprom writes
#define ROTATE_MAGIC_ADDRESS
/* if set, the magic address is used to store the address of the actual magic address.
   If we only change the location of the actual magic address once in a while this address will be written more often but not as often as every boot.
   a good heuristic is to bump the magic address whenever seconds==1. This triggers on average once every 60 bootups. 
*/



#define DS1302_SECONDS_WRITE_ADDRESS 0x80
#define DS1302_MINUTES_WRITE_ADDRESS 0x82
#define DS1302_HOURS_WRITE_ADDRESS 0x84

#define DS1302_SECONDS_READ_ADDRESS 0x81
#define DS1302_MINUTES_READ_ADDRESS 0x83
#define DS1302_HOURS_READ_ADDRESS 0x85

#define DS1302_WRITE_PROTECT_ADDRESS 0x8E


// typedef uint8_t byte

#include <util/delay.h>

int main(){
  asm("cli"); // no interrupts, at all, ever! we don't need them and they mess with debugging
  // just for good measure I added the above but on boot the interrupt flag should default to off

  // disable analog to digital citcuits. saves power
  ADCSRA &= ~(1<<ADEN); 
  // power_spi_disable();
  // power_timer0_disable();
  // power_timer1_disable();
  // power_twi_disable(); 
  // disable uart?

  // TODO: more power saving: 
  // lower clock speed? don't forget to lower BOD level too! otherwise little gain.
  // either use 4.8MHz or 128kHz builtin clock or set a prescaler in software
  // 


// clear write protect bit:
  my_ds1302_write(DS1302_WRITE_PROTECT_ADDRESS,0);


  #ifdef SET_INITIAL_TIME
  const char timestr[] = __TIME__;
  my_ds1302_write(DS1302_HOURS_WRITE_ADDRESS,  ((timestr[0]-48)<<4)+timestr[1]-48); // crude 2-char to bcd
  my_ds1302_write(DS1302_MINUTES_WRITE_ADDRESS,((timestr[3]-48)<<4)+timestr[4]-48);
  my_ds1302_write(DS1302_SECONDS_WRITE_ADDRESS,((timestr[6]-48)<<4)+timestr[7]-48);  
  // hardcoded time:
  // my_ds1302_write(DS1302_HOURS_WRITE_ADDRESS,(1<<4)+9);   // 32 0x21
  // my_ds1302_write(DS1302_MINUTES_WRITE_ADDRESS,(2<<4)+0); // 68 0x47
  // my_ds1302_write(DS1302_SECONDS_WRITE_ADDRESS,(2<<4)+1); // 18 0x55
  #endif



  // read the time:
  #ifdef SELF_SETTING_OF_TIME_THROUGH_QUICK_RESETS
  uint8_t seconds  = bcd2int(my_ds1302_read(DS1302_SECONDS_READ_ADDRESS));  
  #endif
	uint8_t minutes  = bcd2int(my_ds1302_read(DS1302_MINUTES_READ_ADDRESS));  
	uint8_t hours    = bcd2int(my_ds1302_read(DS1302_HOURS_READ_ADDRESS)); 

  
 
  #ifdef SELF_SETTING_OF_TIME_THROUGH_QUICK_RESETS 
  #ifdef ROTATE_MAGIC_ADDRESS
    // there are 31 ram addresses on the DS1302 starting at 0xC0. 
    // The first of which is taken for the magic address address.
    // at first time use the address may be odd so we &~1 to make it even.
    // and it should be in the range [0-58] because we have 30 slots (each with a write and a read address).
    // Just in case the address ram got initialized as something larger than 30 we %30 it
    uint8_t magic_address = 0xC2+((my_ds1302_read(QUICK_RESET_MAGIC_ADDRESS+1)&~1)%60);
    // my_toggle_write(magic_address);
  #else
    // otherwise just use the magic address itself
    uint8_t magic_address = QUICK_RESET_MAGIC_ADDRESS;
  #endif

  uint8_t status = my_ds1302_read(magic_address+1); // read this from the old magic address
  
  // update the magic address
  #ifdef ROTATE_MAGIC_ADDRESS    
  if(seconds==1){  // bump the address
    magic_address += 2;
    if(magic_address>0xFC) magic_address = 0xC2;
    my_ds1302_write(QUICK_RESET_MAGIC_ADDRESS,magic_address-0xC2); // save the offset
  }
  #endif

  my_ds1302_write(magic_address, 3);// 3 indicating first phase
  // my_ds1302_read(magic_address+1);

  if(status==3){ // very fast reboot (leds were still in hour display mode)
    hours = (hours+1)%24;
    my_ds1302_write(DS1302_HOURS_WRITE_ADDRESS,int2bcd(hours));
    
  }


  if(status==2){ // moderately fast reboot (leds were in minute display mode)
    minutes = (minutes+1)%60;
    my_ds1302_write(DS1302_SECONDS_WRITE_ADDRESS,0); // prevent s accidental overflow
    my_ds1302_write(DS1302_MINUTES_WRITE_ADDRESS,int2bcd(minutes));
  }
  
  #endif


  // setting is done, this only counts for display:
  if(hours!=12){ // show 12 noon as 12 (12 midnight is still 0)
    hours %= 12; // otherwise show as [1-11]
  }
 
  // HOURS PHASE
  uint16_t i2;

  if(status!=2){ // skip hour display when we are setting minutes for faster updating
    for(i2=0;i2<500;i2++){

      // pin PB1 HIGH: (CE still Low)
      DDRB  = 0b10010;
      PORTB = 0b00010;
      // set pin 2 only
      if(hours & bit(1)){ DDRB |= bit(2);}
      // hours has no bit 4, otherwise we would have done:
      // if(hours & bit(4)){ DDRB |= bit(3);}
      delay(1);

      // pin PB2 HIGH: (keep CE low)
      DDRB  = 0b10100;
      PORTB = 0b00100;
      // set pin 1 and 3
      if(hours & bit(2)){ DDRB |= bit(1);} 
      if(hours & bit(0)){ DDRB |= bit(3);} 
      delay(1);

      // pin PB3 HIGH: ( and CE low)
      DDRB  = 0b11000;
      PORTB = 0b01000;
      // set pin 2
      if(hours & bit(3)){ DDRB |= bit(2);}
      // hours has no bit 5, otherwise we should have done:
      // if(hours & bit(5)){ DDRB |= bit(1);}
      delay(1);

    }
  }


  
  #ifdef SELF_SETTING_OF_TIME_THROUGH_QUICK_RESETS
  my_ds1302_write(magic_address, 2); // 2 indicating second phase



  // MINUTES PHASE

  // uint16_t i2;
  for(i2=0;i2<500;i2++){

    // pin PB1 HIGH: (CE still Low)
    DDRB  = 0b10010;
    PORTB = 0b00010;
    // set pin 2 and 3
    if(minutes & bit(1)){ DDRB |= bit(2);}
    if(minutes & bit(4)){ DDRB |= bit(3);}
    delay(1);

    // pin PB2 HIGH: (keep CE low)
    DDRB  = 0b10100;
    PORTB = 0b00100;
    // set pin 1 and 3
    if(minutes & bit(2)){ DDRB |= bit(1);} 
    if(minutes & bit(0)){ DDRB |= bit(3);} 
    delay(1);

    // pin PB3 HIGH: ( and CE low)
    DDRB  = 0b11000;
    PORTB = 0b01000;
    // set pin 1 and 2
    if(minutes & bit(3)){ DDRB |= bit(2);}
    if(minutes & bit(5)){ DDRB |= bit(1);}
    delay(1);
  }


  my_ds1302_write(magic_address, 0); // 0 indicates all done
  #endif


  // set all to input except DS1302 CE pin, which is OUTPUT and LOW
  DDRB  = 0b10000;
  // necessary because it enables pullups which reduce power conumption by the analog comparators that would otherwise product interrupts
	// source: http://www.atmel.com/Images/doc8349.pdf 
  PORTB = 0b11111; 
  

  // TODO: investigate BOD: appently the following disables BOD only while sleeping?:
  // sleep_bod_disable() 

  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
  sleep_enable();            
  sleep_mode(); 
}



// does not release pins!
void my_toggle_write(uint8_t data){
  uint8_t i;
  for(i = 0; i <= 7; i++) { 
    // CLK low
    PORTB &= ~0b00100;
    // write bit to IO pin, keep CE HIGH and the rest LOW
    if(data & 0x01){ PORTB = 0b11000; }else{ PORTB = 0b10000; }
    _delay_us(1);
    // CLK high
    PORTB |= 0b00100;
    _delay_us(1);
    
    data >>=1;
  }
}


// always used after a my_toggle_write, 
// so CLK is still HIGH
// DS1302 will transmit first bit on LOW so start with a LOW
uint8_t my_toggle_read(){
  uint8_t i, data;

  data = 0;
  for( i = 0; i <= 7; i++) {
    // Clock down, data is ready after some time.
    // digitalWrite( DS1302_SCLK_PIN, LOW);
    PORTB &= ~0b00100;
    _delay_us(1);
    // PORTB |=  0b00001;
    
    // read bit, and set it in place in 'data' variable
    // data <<= 1;
    // data += !!(PINB & 0b01000);
    data += ((PINB & 0b01000) >> 3) << i; 
    // bitWrite( data, i, digitalRead( DS1302_IO_PIN)); 
    // PORTB &= ~0b00001;

    // Issue a clock pulse for the next databit.
    PORTB |= 0b00100;
    _delay_us(1);
    
  }
  return data;
}



uint8_t my_ds1302_read(uint8_t address){
  
  // digitalWrite( DS1302_CE_PIN, LOW); // default, not enabled
  // pinMode( DS1302_CE_PIN, OUTPUT);  
  // digitalWrite( DS1302_SCLK_PIN, LOW); // default, clock low
  // pinMode( DS1302_SCLK_PIN, OUTPUT);
  // pinMode( DS1302_IO_PIN, OUTPUT);
  // digitalWrite( DS1302_CE_PIN, HIGH); // start the session

  PORTB = 0b10000; // CE HIGH, rest low 
  DDRB  = 0b11111; // sets CE, CLK and IO as output
  // allow DS1302 to boot up
  _delay_us(4);

  // write address:
  my_toggle_write(address);

  // release IO line, keep CE and CLK as OUTPUTs
  // pinMode( DS1302_IO_PIN, INPUT);
  DDRB = 0b10111;
  // read the value:
  uint8_t data;
  data = my_toggle_read();

  // disable DS1302
  // digitalWrite( DS1302_CE_PIN, LOW);
  // and let it cool down...
  PORTB &= ~0b10000;
  _delay_us(4);
  
  return data;
}



void my_ds1302_write(uint8_t address,uint8_t data){
  
  // digitalWrite( DS1302_CE_PIN, LOW); // default, not enabled
  // pinMode( DS1302_CE_PIN, OUTPUT);  
  // digitalWrite( DS1302_SCLK_PIN, LOW); // default, clock low
  // pinMode( DS1302_SCLK_PIN, OUTPUT);
  // pinMode( DS1302_IO_PIN, OUTPUT);
  // digitalWrite( DS1302_CE_PIN, HIGH); // start the session

  PORTB = 0b10000; // CE HIGH, rest low 
  DDRB  = 0b11111;  // sets CE, CLK and IO as output
  // allow DS1302 to boot up:
  _delay_us(4);

  // write address:
  my_toggle_write(address);
  my_toggle_write(data);

  // disable DS1302
  // digitalWrite( DS1302_CE_PIN, LOW);
  // and let it cool down...
  PORTB &= ~0b10000;
  _delay_us(4);
}
