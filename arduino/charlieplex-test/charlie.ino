/*
||
|| @author Alexander Brevig
|| @version 1.0
||
*/
/*
This code is tested with built in led @ pin 13, a led connected with anode 12 cathode 13, and a last led connected with anode 11 cathode 13 
I would love feedback at alexanderbrevig@gmail.com
*/

#include <Chaplex.h>

#define NUMBER_OF_PINS 4
//define pins in the order you want to adress them
byte pins[] = {PB3,PB2,PB1,PB0};

//initialize object
Chaplex charlieplex = Chaplex(pins,NUMBER_OF_PINS);

charlieLed ledM1 =  { 1 , 0 }; 
charlieLed ledM2 =  { 2 , 1 };
charlieLed ledM4 =  { 3 , 2 };
charlieLed ledM8 =  { 1 , 3 };
charlieLed ledM16 = { 0 , 3 };
charlieLed ledM32 = { 3 , 0 };
charlieLed ledH1 =  { 0 , 1 };
charlieLed ledH2 =  { 1 , 2 };
charlieLed ledH4 =  { 2 , 3 };
charlieLed ledH8 =  { 3 , 1 };


charlieLed leds[10] = { ledM1, ledM2, ledM4, ledM8, ledM16, ledM32, ledH1, ledH2, ledH4, ledH8 };


void setup(){
	charlieplex.allClear(); 
	// charlieplex.ledWrite(ledM1,ON);
}

void loop(){
	// charlieplex.outRow();
  // if (singleOn){ charlieplex.clear(); }
  
  	charlieplex.allClear(); 
  	// for(int i=0;i<8000;i++){ charlieplex.outRow(); }

 	// for(int led=0;led<10;led++){
  //   	charlieplex.ledWrite(leds[led], ON);
  //   }

  	// for(int i=0;i<8000;i++){ charlieplex.outRow(); }


  for(int led=0;led<10;led++){
    charlieplex.ledWrite(leds[led], ON);
    for(int i=0;i<4000;i++){ charlieplex.outRow(); }
 }
}

