
#include <avr/sleep.h>
#include <avr/power.h>
// #include <Serial.h>

// Set your own pins with these defines !
#define DS1302_SCLK_PIN   2    // Arduino pin for the Serial Clock
#define DS1302_IO_PIN     3    // Arduino pin for the Data I/O
#define DS1302_CE_PIN     4    // Arduino pin for the Chip Enable



// Register names.
// Since the highest bit is always '1', 
// the registers start at 0x80
// If the register is read, the lowest bit should be '1'.
#define DS1302_SECONDS           0x80
#define DS1302_MINUTES           0x82
#define DS1302_HOURS             0x84
#define DS1302_DATE              0x86
#define DS1302_MONTH             0x88
#define DS1302_DAY               0x8A
#define DS1302_YEAR              0x8C
#define DS1302_ENABLE            0x8E
#define DS1302_TRICKLE           0x90
#define DS1302_CLOCK_BURST       0xBE
#define DS1302_CLOCK_BURST_WRITE 0xBE
#define DS1302_CLOCK_BURST_READ  0xBF
#define DS1302_RAMSTART          0xC0
#define DS1302_RAMEND            0xFC
#define DS1302_RAM_BURST         0xFE
#define DS1302_RAM_BURST_WRITE   0xFE
#define DS1302_RAM_BURST_READ    0xFF



// Defines for the bits, to be able to change 
// between bit number and binary definition.
// By using the bit number, using the DS1302 
// is like programming an AVR microcontroller.
// But instead of using "(1<<X)", or "_BV(X)", 
// the Arduino "bit(X)" is used.
#define DS1302_D0 0
#define DS1302_D1 1
#define DS1302_D2 2
#define DS1302_D3 3
#define DS1302_D4 4
#define DS1302_D5 5
#define DS1302_D6 6
#define DS1302_D7 7


// Bit for reading (bit in address)
#define DS1302_READBIT DS1302_D0 // READBIT=1: read instruction

// Bit for clock (0) or ram (1) area, 
// called R/C-bit (bit in address)
#define DS1302_RC DS1302_D6

// Seconds Register
#define DS1302_CH DS1302_D7   // 1 = Clock Halt, 0 = start

// Hour Register
#define DS1302_AM_PM DS1302_D5 // 0 = AM, 1 = PM
#define DS1302_12_24 DS1302_D7 // 0 = 24 hour, 1 = 12 hour

// Enable Register
#define DS1302_WP DS1302_D7   // 1 = Write Protect, 0 = enabled

// Trickle Register
#define DS1302_ROUT0 DS1302_D0
#define DS1302_ROUT1 DS1302_D1
#define DS1302_DS0   DS1302_D2
#define DS1302_DS1   DS1302_D2
#define DS1302_TCS0  DS1302_D4
#define DS1302_TCS1  DS1302_D5
#define DS1302_TCS2  DS1302_D6
#define DS1302_TCS3  DS1302_D7


#define LED0 PB3
#define LED1 PB4
#define TIME 200



void setup(){
	// lights off:
	pinMode(LED0,OUTPUT);
	pinMode(LED1,OUTPUT);
  Serial.begin(9600);
  // DS1302_write(DS1302_SECONDS,5);
  // DS1302_write(DS1302_MINUTES,6);
  DS1302_write(DS1302_HOURS,4);
  Serial.println("setup done");

}

void loop(){
	// read the actual registers:
  byte hourdata = DS1302_read(DS1302_HOURS);
  byte mindata  = DS1302_read(DS1302_MINUTES);
  byte secdata  = DS1302_read(DS1302_SECONDS);

  Serial.print(hourdata,BIN);
  Serial.print(":");
  Serial.print(mindata);
  Serial.print(":");
  Serial.print(secdata);
  Serial.println();
  // // every decimal of the hours and minutes is encoded separately in 1/2 a byte:
  // byte minutes  = ((mindata >> 4)*10+(mindata & 0xf))/5;
  // byte hours    = ((hourdata >> 4)*10+(hourdata & 0xf))%12;

  // // flash it out:
  // flashout(hours);
  // delay(2*TIME);
  // flashout(minutes);
  delay(1000);
}




void flashout(uint8_t data){
	for(byte i=0;i<4;i++){
		digitalWrite((data & (1<<(3-i)))?LED0:LED1, HIGH);
		delay(TIME);
		digitalWrite((data & (1<<(3-i)))?LED0:LED1, LOW);	
		delay(TIME);
	}
}




// --------------------------------------------------------
// _DS1302_toggleread
//
// A helper function for reading a byte with bit toggle
//
// This function assumes that the SCLK is still high.
//
uint8_t _DS1302_toggleread( void)
{
  uint8_t i, data;

  data = 0;
  for( i = 0; i <= 7; i++)
  {
    // Issue a clock pulse for the next databit.
    // If the 'togglewrite' function was used before 
    // this function, the SCLK is already high.
    digitalWrite( DS1302_SCLK_PIN, HIGH);
    delayMicroseconds( 1);

    // Clock down, data is ready after some time.
    digitalWrite( DS1302_SCLK_PIN, LOW);
    delayMicroseconds( 1);        // tCL=1000ns, tCDD=800ns

    // read bit, and set it in place in 'data' variable
    bitWrite( data, i, digitalRead( DS1302_IO_PIN)); 
  }
  return( data);
}






// --------------------------------------------------------
// _DS1302_togglewrite

// !!! changed version with release hardcoded to true!!!
// //
// void _DS1302_togglewrite( uint8_t data)
// {
//   for(int i = 0; i <= 6; i++) { 
//     // set a bit of the data on the I/O-line
//     digitalWrite( DS1302_IO_PIN, bitRead(data, i));  
//     delayMicroseconds( 1);     // tDC = 200ns

//     // clock up, data is read by DS1302
//     digitalWrite( DS1302_SCLK_PIN, HIGH);     
//     delayMicroseconds( 1);     // tCH = 1000ns, tCDH = 800ns

//     digitalWrite( DS1302_SCLK_PIN, LOW);
//     delayMicroseconds( 1);       // tCL=1000ns, tCDD=800ns
//   }
//   pinMode( DS1302_IO_PIN, INPUT);
// }


// --------------------------------------------------------
// DS1302_read
//
// !!! HEAVILY INLINED!
//
uint8_t DS1302_read(uint8_t address)
{
  // uint8_t data;

  // set lowest bit (read bit) in address
  bitSet( address, DS1302_READBIT);  
  digitalWrite( DS1302_CE_PIN, LOW); // default, not enabled
  pinMode( DS1302_CE_PIN, OUTPUT);  
  digitalWrite( DS1302_SCLK_PIN, LOW); // default, clock low
  pinMode( DS1302_SCLK_PIN, OUTPUT);
  pinMode( DS1302_IO_PIN, OUTPUT);
  digitalWrite( DS1302_CE_PIN, HIGH); // start the session
  delayMicroseconds( 4);           // tCC = 4us

  // inlined version of toggleWrite():
  // the I/O-line is released for the data
  for(byte i = 0; i <= 6; i++) { 
    // set a bit of the data on the I/O-line
    digitalWrite( DS1302_IO_PIN, bitRead(address, i));  
    delayMicroseconds( 1);     // tDC = 200ns

    // clock up, address is read by DS1302
    digitalWrite( DS1302_SCLK_PIN, HIGH);     
    delayMicroseconds( 1);     // tCH = 1000ns, tCDH = 800ns

    digitalWrite( DS1302_SCLK_PIN, LOW);
    delayMicroseconds( 1);       // tCL=1000ns, tCDD=800ns
  }
  pinMode( DS1302_IO_PIN, INPUT);
  // _DS1302_togglewrite( address);  
  // inlined toggleread
  // data = _DS1302_toggleread();
  uint8_t data = 0;
  for(byte i = 0; i <= 7; i++)
  {
    // for(byte i=0;i<2;i++){
    //   digitalWrite( DS1302_SCLK_PIN, i?HIGH:LOW);
    //   delayMicroseconds( 1);
    // }
    digitalWrite( DS1302_SCLK_PIN, HIGH);
    delayMicroseconds( 1);
    digitalWrite( DS1302_SCLK_PIN, LOW);
    delayMicroseconds( 1);
    // read bit, and set it in place in 'data' variable
    bitWrite( data, i, digitalRead( DS1302_IO_PIN)); 
  }
  delayMicroseconds( 4);           // tCWH = 4us

  return (data);
}


void DS1302_write( int address, uint8_t data)
{
  // clear lowest bit (read bit) in address
  bitClear( address, DS1302_READBIT);   
  digitalWrite( DS1302_CE_PIN, LOW); // default, not enabled
  pinMode( DS1302_CE_PIN, OUTPUT);  
  digitalWrite( DS1302_SCLK_PIN, LOW); // default, clock low
  pinMode( DS1302_SCLK_PIN, OUTPUT);
  pinMode( DS1302_IO_PIN, OUTPUT);

  digitalWrite( DS1302_CE_PIN, HIGH); // start the session
  delayMicroseconds( 4);           // tCC = 4us
  // don't release the I/O-line
  _DS1302_togglewrite( address, false); 
  // don't release the I/O-line
  _DS1302_togglewrite( data, false); 

  digitalWrite( DS1302_CE_PIN, LOW);
  delayMicroseconds( 4);           // tCWH = 4us
}


// --------------------------------------------------------
// _DS1302_togglewrite
//
// A helper function for writing a byte with bit toggle
//
// The 'release' parameter is for a read after this write.
// It will release the I/O-line and will keep the SCLK high.
//
void _DS1302_togglewrite( uint8_t data, uint8_t release)
{
  int i;

  for( i = 0; i <= 7; i++)
  { 
    // set a bit of the data on the I/O-line
    digitalWrite( DS1302_IO_PIN, bitRead(data, i));  
    delayMicroseconds( 1);     // tDC = 200ns

    // clock up, data is read by DS1302
    digitalWrite( DS1302_SCLK_PIN, HIGH);     
    delayMicroseconds( 1);     // tCH = 1000ns, tCDH = 800ns

    if( release && i == 7)
    {
      // If this write is followed by a read, 
      // the I/O-line should be released after 
      // the last bit, before the clock line is made low.
      // This is according the datasheet.
      // I have seen other programs that don't release 
      // the I/O-line at this moment,
      // and that could cause a shortcut spike 
      // on the I/O-line.
      pinMode( DS1302_IO_PIN, INPUT);

      // For Arduino 1.0.3, removing the pull-up is no longer needed.
      // Setting the pin as 'INPUT' will already remove the pull-up.
      // digitalWrite (DS1302_IO, LOW); // remove any pull-up  
    }
    else
    {
      digitalWrite( DS1302_SCLK_PIN, LOW);
      delayMicroseconds( 1);       // tCL=1000ns, tCDD=800ns
    }
  }
}
