
#include <avr/sleep.h>
#include <avr/power.h>

// Set your own pins with these defines !
#define DS1302_SCLK_PIN   PB2    // Arduino pin for the Serial Clock
#define DS1302_IO_PIN     PB3    // Arduino pin for the Data I/O
#define DS1302_CE_PIN     PB4    // Arduino pin for the Chip Enable



// Register names.
// Since the highest bit is always '1', 
// the registers start at 0x80
// If the register is read, the lowest bit should be '1'.
#define DS1302_SECONDS           0x80
#define DS1302_MINUTES           0x82
#define DS1302_HOURS             0x84
#define DS1302_DATE              0x86
#define DS1302_MONTH             0x88
#define DS1302_DAY               0x8A
#define DS1302_YEAR              0x8C
#define DS1302_ENABLE            0x8E
#define DS1302_TRICKLE           0x90
#define DS1302_CLOCK_BURST       0xBE
#define DS1302_CLOCK_BURST_WRITE 0xBE
#define DS1302_CLOCK_BURST_READ  0xBF
#define DS1302_RAMSTART          0xC0
#define DS1302_RAMEND            0xFC
#define DS1302_RAM_BURST         0xFE
#define DS1302_RAM_BURST_WRITE   0xFE
#define DS1302_RAM_BURST_READ    0xFF



// Defines for the bits, to be able to change 
// between bit number and binary definition.
// By using the bit number, using the DS1302 
// is like programming an AVR microcontroller.
// But instead of using "(1<<X)", or "_BV(X)", 
// the Arduino "bit(X)" is used.
#define DS1302_D0 0
#define DS1302_D1 1
#define DS1302_D2 2
#define DS1302_D3 3
#define DS1302_D4 4
#define DS1302_D5 5
#define DS1302_D6 6
#define DS1302_D7 7


// Bit for reading (bit in address)
#define DS1302_READBIT DS1302_D0 // READBIT=1: read instruction

// Bit for clock (0) or ram (1) area, 
// called R/C-bit (bit in address)
#define DS1302_RC DS1302_D6

// Seconds Register
#define DS1302_CH DS1302_D7   // 1 = Clock Halt, 0 = start

// Hour Register
#define DS1302_AM_PM DS1302_D5 // 0 = AM, 1 = PM
#define DS1302_12_24 DS1302_D7 // 0 = 24 hour, 1 = 12 hour

// Enable Register
#define DS1302_WP DS1302_D7   // 1 = Write Protect, 0 = enabled

// Trickle Register
#define DS1302_ROUT0 DS1302_D0
#define DS1302_ROUT1 DS1302_D1
#define DS1302_DS0   DS1302_D2
#define DS1302_DS1   DS1302_D2
#define DS1302_TCS0  DS1302_D4
#define DS1302_TCS1  DS1302_D5
#define DS1302_TCS2  DS1302_D6
#define DS1302_TCS3  DS1302_D7


#define ITERATIONS 16000

#include <Chaplex.h>

#define NUMBER_OF_PINS 4
//define pins in the order you want to adress them
byte pins[] = {PB3,PB2,PB1,PB0};
// byte pins[] = {11,10,9,8};

//initialize object
Chaplex charlieplex = Chaplex(pins,NUMBER_OF_PINS);

charlieLed ledM1 =  { 1 , 0 }; 
charlieLed ledM2 =  { 2 , 1 };
charlieLed ledM4 =  { 3 , 2 };
charlieLed ledM8 =  { 1 , 3 };
charlieLed ledM16 = { 0 , 3 };
charlieLed ledM32 = { 3 , 0 };
charlieLed ledH1 =  { 0 , 1 };
charlieLed ledH2 =  { 1 , 2 };
charlieLed ledH4 =  { 2 , 3 };
charlieLed ledH8 =  { 3 , 1 };
// 

void setup(){
  ADCSRA &= ~(1<<ADEN); // disable analog to digital citcuits. saves power

  // #define DOSETTIME
  #ifdef DOSETTIME
  // if(DS1302_read(DS1302_YEAR) < 10){ // defaults to 0
    DS1302_write(DS1302_YEAR,    15);
    DS1302_write(DS1302_MONTH,   7 );
    DS1302_write(DS1302_DATE,    13);
    DS1302_write(DS1302_HOURS,   (1<<4) + 1 );  // 7 hours
    DS1302_write(DS1302_MINUTES, (3<<4) + 0 );  // 3 minutes

  // required to start the clock
    DS1302_write(DS1302_ENABLE,  0);
    DS1302_write(DS1302_SECONDS, 0); 
  // }
  #endif


	// // // read the actual registers:
	byte hourdata = DS1302_read(DS1302_HOURS);
	byte mindata  = DS1302_read(DS1302_MINUTES);
  // byte secdata  = DS1302_read(DS1302_SECONDS);

 // //  // disable ds1302 communication:
  pinMode(DS1302_CE_PIN, OUTPUT);
  digitalWrite(DS1302_CE_PIN, LOW);


	// // // every decimal of the hours and minutes is encoded separately in 1/2 a byte:
	byte minutes  = ((mindata >> 4)*10+(mindata & 0xf));
	byte hours    = ((hourdata >> 4)*10+(hourdata & 0xf));
  hours %= 12; // saves one LED
  // byte seconds  = ((secdata >> 4)*10+(secdata & 0xf));

	// // display the time:
  charlieplex.allClear();
  charlieplex.ledWrite(ledM1, (minutes & bit(0))?ON:OFF);
  charlieplex.ledWrite(ledM2, (minutes & bit(1))?ON:OFF);
  charlieplex.ledWrite(ledM4, (minutes & bit(2))?ON:OFF);
  charlieplex.ledWrite(ledM8, (minutes & bit(3))?ON:OFF);
  charlieplex.ledWrite(ledM16,(minutes & bit(4))?ON:OFF);
  charlieplex.ledWrite(ledM32,(minutes & bit(5))?ON:OFF);
  charlieplex.ledWrite(ledH1, (hours & bit(0))?ON:OFF);
  charlieplex.ledWrite(ledH2, (hours & bit(1))?ON:OFF);
  charlieplex.ledWrite(ledH4, (hours & bit(2))?ON:OFF);
  charlieplex.ledWrite(ledH8, (hours & bit(3))?ON:OFF);


  // show time for some time:
  for(long i=0;i<64000;i++){
    charlieplex.outRow(); 
  }

  // clear the display:
  for(byte i=0;i<4;i++){
    pinMode(pins[i], INPUT);
    // digitalWrite(pins[i], LOW);
  }

	// go to sleep:
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
  sleep_enable();            
  sleep_mode(); 
}

void loop(){

}



// --------------------------------------------------------
// DS1302_read
//
// This function reads a byte from the DS1302 
// (clock or ram).
//
// The address could be like "0x80" or "0x81", 
// the lowest bit is set anyway.
//
// This function may be called as the first function, 
// also the pinMode is set.
//
uint8_t DS1302_read(int address)
{
  uint8_t data;

  // set lowest bit (read bit) in address
  bitSet( address, DS1302_READBIT);  

  _DS1302_start();
  // the I/O-line is released for the data
  _DS1302_togglewrite( address, true);  
  data = _DS1302_toggleread();
  _DS1302_stop();

  return (data);
}


// --------------------------------------------------------
// DS1302_write
//
// This function writes a byte to the DS1302 (clock or ram).
//
// The address could be like "0x80" or "0x81", 
// the lowest bit is cleared anyway.
//
// This function may be called as the first function, 
// also the pinMode is set.
//
void DS1302_write( int address, uint8_t data)
{
  // clear lowest bit (read bit) in address
  bitClear( address, DS1302_READBIT);   

  _DS1302_start();
  // don't release the I/O-line
  _DS1302_togglewrite( address, false); 
  // don't release the I/O-line
  _DS1302_togglewrite( data, false); 
  _DS1302_stop();  
}


// --------------------------------------------------------
// _DS1302_start
//
// A helper function to setup the start condition.
//
// An 'init' function is not used.
// But now the pinMode is set every time.
// That's not a big deal, and it's valid.
// At startup, the pins of the Arduino are high impedance.
// Since the DS1302 has pull-down resistors, 
// the signals are low (inactive) until the DS1302 is used.
void _DS1302_start( void)
{
  digitalWrite( DS1302_CE_PIN, LOW); // default, not enabled
  pinMode( DS1302_CE_PIN, OUTPUT);  

  digitalWrite( DS1302_SCLK_PIN, LOW); // default, clock low
  pinMode( DS1302_SCLK_PIN, OUTPUT);

  pinMode( DS1302_IO_PIN, OUTPUT);

  digitalWrite( DS1302_CE_PIN, HIGH); // start the session
  delayMicroseconds( 4);           // tCC = 4us
}


// --------------------------------------------------------
// _DS1302_stop
//
// A helper function to finish the communication.
//
void _DS1302_stop(void)
{
  // Set CE low
  digitalWrite( DS1302_CE_PIN, LOW);

  delayMicroseconds( 4);           // tCWH = 4us
}


// --------------------------------------------------------
// _DS1302_toggleread
//
// A helper function for reading a byte with bit toggle
//
// This function assumes that the SCLK is still high.
//
uint8_t _DS1302_toggleread( void)
{
  uint8_t i, data;

  data = 0;
  for( i = 0; i <= 7; i++)
  {
    // Issue a clock pulse for the next databit.
    // If the 'togglewrite' function was used before 
    // this function, the SCLK is already high.
    digitalWrite( DS1302_SCLK_PIN, HIGH);
    delayMicroseconds( 1);

    // Clock down, data is ready after some time.
    digitalWrite( DS1302_SCLK_PIN, LOW);
    delayMicroseconds( 1);        // tCL=1000ns, tCDD=800ns

    // read bit, and set it in place in 'data' variable
    bitWrite( data, i, digitalRead( DS1302_IO_PIN)); 
  }
  return( data);
}


// --------------------------------------------------------
// _DS1302_togglewrite
//
// A helper function for writing a byte with bit toggle
//
// The 'release' parameter is for a read after this write.
// It will release the I/O-line and will keep the SCLK high.
//
void _DS1302_togglewrite( uint8_t data, uint8_t release)
{
  int i;

  for( i = 0; i <= 7; i++)
  { 
    // set a bit of the data on the I/O-line
    digitalWrite( DS1302_IO_PIN, bitRead(data, i));  
    delayMicroseconds( 1);     // tDC = 200ns

    // clock up, data is read by DS1302
    digitalWrite( DS1302_SCLK_PIN, HIGH);     
    delayMicroseconds( 1);     // tCH = 1000ns, tCDH = 800ns

    if( release && i == 7)
    {
      // If this write is followed by a read, 
      // the I/O-line should be released after 
      // the last bit, before the clock line is made low.
      // This is according the datasheet.
      // I have seen other programs that don't release 
      // the I/O-line at this moment,
      // and that could cause a shortcut spike 
      // on the I/O-line.
      pinMode( DS1302_IO_PIN, INPUT);

      // For Arduino 1.0.3, removing the pull-up is no longer needed.
      // Setting the pin as 'INPUT' will already remove the pull-up.
      // digitalWrite (DS1302_IO, LOW); // remove any pull-up  
    }
    else
    {
      digitalWrite( DS1302_SCLK_PIN, LOW);
      delayMicroseconds( 1);       // tCL=1000ns, tCDD=800ns
    }
  }
}


// // --------------------------------------------------------
// // DS1302_read
// //
// // !!! HEAVILY INLINED!
// //
// uint8_t DS1302_read(uint8_t address)
// {
//   // uint8_t data;

//   // set lowest bit (read bit) in address
//   bitSet( address, DS1302_READBIT);  
//   digitalWrite( DS1302_CE_PIN, LOW); // default, not enabled
//   pinMode( DS1302_CE_PIN, OUTPUT);  
//   digitalWrite( DS1302_SCLK_PIN, LOW); // default, clock low
//   pinMode( DS1302_SCLK_PIN, OUTPUT);
//   pinMode( DS1302_IO_PIN, OUTPUT);
//   digitalWrite( DS1302_CE_PIN, HIGH); // start the session
//   delayMicroseconds( 4);           // tCC = 4us

//   // inlined version of toggleWrite():
//   // the I/O-line is released for the data
//   for(byte i = 0; i <= 6; i++) { 
//     // set a bit of the data on the I/O-line
//     digitalWrite( DS1302_IO_PIN, bitRead(address, i));  
//     delayMicroseconds( 1);     // tDC = 200ns

//     // clock up, address is read by DS1302
//     digitalWrite( DS1302_SCLK_PIN, HIGH);     
//     delayMicroseconds( 1);     // tCH = 1000ns, tCDH = 800ns

//     digitalWrite( DS1302_SCLK_PIN, LOW);
//     delayMicroseconds( 1);       // tCL=1000ns, tCDD=800ns
//   }
//   pinMode( DS1302_IO_PIN, INPUT);
//   // _DS1302_togglewrite( address);  
//   // inlined toggleread
//   // data = _DS1302_toggleread();
//   uint8_t data = 0;
//   for(byte i = 0; i <= 7; i++)
//   {
//     digitalWrite( DS1302_SCLK_PIN, HIGH);
//     delayMicroseconds( 1);
//     digitalWrite( DS1302_SCLK_PIN, LOW);
//     delayMicroseconds( 1);
//     // read bit, and set it in place in 'data' variable
//     bitWrite( data, i, digitalRead( DS1302_IO_PIN)); 
//   }
//   delayMicroseconds( 4);           // tCWH = 4us

//   return (data);
// }


