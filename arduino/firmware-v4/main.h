#ifndef _MAIN_H
#define _MAIN_H

#include <inttypes.h>
#include <avr/delay.h>

int main();

void my_toggle_write(uint8_t data);
uint8_t my_toggle_read();
uint8_t my_ds1302_read(uint8_t address);
void my_ds1302_write(uint8_t address,uint8_t data);

// some arduino function that we really, really like:
// inaccurate ms delay
void delay(unsigned ms){
    while(ms--){
        _delay_ms(1); //Using the libc routine over and over is non-optimal but it works and is close enough
    }
}

#define bit(b) (1u << (b))

//#define bcd2int(bcd) (((bcd) >> 4)*10+((bcd)&0xf))
uint8_t bcd2int(uint8_t bcd){
	return (bcd >> 4)*10+(bcd&0xf);
}

// #define int2bcd ((((in)/10)<<4) + ((in)%10))
uint8_t int2bcd(uint8_t in){
	return ((in/10)<<4) + (in%10);
}

#endif