
#define BLINKPIN1 PB2
#define BLINKPIN2 PB3

void setup(){
	pinMode(BLINKPIN1, OUTPUT);
	pinMode(BLINKPIN2, OUTPUT);
}

void loop(){
	digitalWrite(BLINKPIN1, HIGH);
	digitalWrite(BLINKPIN2, LOW);
	delay(1000);
	digitalWrite(BLINKPIN1, LOW);
	digitalWrite(BLINKPIN2, HIGH);
	delay(1000);
}